class RemoveNetworkFromDataCenters < ActiveRecord::Migration[6.0]
  def change
    remove_column :data_centers, :network
  end
end
