class CreateDataCenters < ActiveRecord::Migration[6.0]
  def change
    create_table :data_centers do |t|
      t.float  :latitude
      t.float  :longitude
      t.string :network
      t.string :city
      t.string :country

      t.timestamps
    end
  end
end
