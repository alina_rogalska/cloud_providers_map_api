class CreateIpRanges < ActiveRecord::Migration[6.0]
  def change
    create_table :ip_ranges do |t|
      t.string :network
      t.string :range
      t.references :company, foreign_key: true, index: true
      t.references :data_center, foreign_key: true, index: true

      t.timestamps
    end
  end
end
