require 'json'

IpRange.destroy_all
DataCenter.destroy_all
Company.destroy_all

companies_file = File.read(Rails.root.join("./companies.json"))
companies_data = JSON.parse(companies_file)

companies_data.each do |company|
  Company.create(
    name:  company["name"],
    website:  company["website"],
    wikipedia_link:  company["wikipedia_link"],
    country:  company["country"],
    description:  company["description"]
  )
end

companies_names = %w(amazon cloudflare microsoft oracle)

companies_names.each do |name|
  file = File.read(Rails.root.join("./#{name}_data.json"))
  data = JSON.parse(file)
  company = Company.where('lower(name) LIKE ?', "#{name}%").take

  data.each do |ip_range|
    data_center = DataCenter.find_or_create_by(latitude:  ip_range["latitude"].to_f,
                                               longitude: ip_range["longitude"].to_f,
                                               city:      ip_range["city"],
                                               country:   ip_range["country_name_geo"])

    range = IpRange.new(network:  ip_range["ip_range"], range: ip_range["inetnum"])
    range.company = company
    range.data_center = data_center
    range.save!
  end
end
