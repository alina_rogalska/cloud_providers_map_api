Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :data_centers, only: [:index, :show]
  resources :ip_ranges, only: :index
  resources :companies, only: [:index, :show]
end
