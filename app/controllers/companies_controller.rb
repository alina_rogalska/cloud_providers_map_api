class CompaniesController < ApplicationController
  def index
    render json: Company.all, each_serializer: SimpleCompanySerializer
  end

  def show
    company = Company.find(params[:id])

    render json: company
  end
end
