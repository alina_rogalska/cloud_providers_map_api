class IpRangesController < ApplicationController
  def index
    ip_ranges = IpRangeSearch.new(params).call

    render json: ip_ranges, each_serializer: IpRangeSerializer
  end
end
