class DataCentersController < ApplicationController

  def index
    company_id = params.fetch(:company_id) { nil }
    data_centers = DataCenter.includes(ip_ranges: :company).from_last_update
    data_centers = data_centers.by_company(company_id) if company_id.present?

    render json: data_centers, each_serializer: DataCenterSerializer
  end

  def show
    date = params.fetch(:date) { nil }
    last_update = IpRange.last_update_date(date)&.to_datetime

    render json: {
      data_center: ActiveModelSerializers::SerializableResource.new(
                    DataCenter.find(params[:id]),
                    serializer: DataCenterSerializer,
                    date: date).as_json,
      date: last_update
    }
  end
end
