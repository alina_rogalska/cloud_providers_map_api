# == Schema Information
#
# Table name: ip_ranges
#
#  id             :bigint           not null, primary key
#  network        :string
#  range          :string
#  company_id     :bigint
#  data_center_id :bigint
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  version        :integer
#

class IpRange < ApplicationRecord
  belongs_to :company
  belongs_to :data_center, touch: true

  scope :for_data_center, -> (id) { where(data_center_id: id) }
  scope :for_company,     -> (id) { where(company_id: id) }

  def self.from_last_update(date)
    last_update = last_update_date(date)
    return IpRange.none unless last_update
    where(updated_at: last_update.beginning_of_day..last_update.end_of_day)
  end

  def self.last_update_date(date)
    result = date ? where('updated_at < ?', "#{date.to_date.end_of_day}") : all
    return if result.empty?
    result.order(:updated_at).map{ |range| range.updated_at.to_date }.uniq&.last
  end
end
