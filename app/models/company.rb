# == Schema Information
#
# Table name: companies
#
#  id             :bigint           not null, primary key
#  name           :string
#  website        :string
#  wikipedia_link :string
#  description    :string
#  country        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Company < ApplicationRecord
  has_many :ip_ranges, dependent: :destroy
end
