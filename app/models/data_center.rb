# == Schema Information
#
# Table name: data_centers
#
#  id         :bigint           not null, primary key
#  latitude   :float
#  longitude  :float
#  city       :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DataCenter < ApplicationRecord
  has_many :ip_ranges, dependent: :destroy

  scope :by_company, -> (company_id) { includes(:ip_ranges).where(ip_ranges: { company_id: company_id }) }

  def self.from_last_update
    date = last_update_date
    where(updated_at: date.beginning_of_day..date.end_of_day)
  end

  def self.last_update_date
    order(:updated_at).map{ |data_center| data_center.updated_at.to_date }.uniq&.last
  end
end
