class IpRangeSearch
  attr_accessor :company_id, :data_center_id, :date

  def initialize(params)
    @company_id = params.fetch(:company_id) { nil }
    @data_center_id = params.fetch(:data_center_id) { nil }
    @date = params.fetch(:date) { nil }
  end

  def self.call(params)
    new(params).call
  end

  def call
    ip_ranges = IpRange.includes(:company).from_last_update(date)
    ip_ranges = ip_ranges.for_data_center(data_center_id) if data_center_id.present?
    ip_ranges = ip_ranges.for_company(company_id) if company_id.present?
    ip_ranges
  end
end
