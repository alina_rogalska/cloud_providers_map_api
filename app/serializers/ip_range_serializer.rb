# == Schema Information
#
# Table name: ip_ranges
#
#  id             :bigint           not null, primary key
#  network        :string
#  range          :string
#  company_id     :bigint
#  data_center_id :bigint
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  version        :integer
#

class IpRangeSerializer < ActiveModel::Serializer
  attributes :id, :network, :range, :company_id, :company_name

  def company_name
    object.company&.name
  end
end
