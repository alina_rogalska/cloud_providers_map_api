# == Schema Information
#
# Table name: data_centers
#
#  id         :bigint           not null, primary key
#  latitude   :float
#  longitude  :float
#  city       :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DataCenterSerializer < ActiveModel::Serializer
  attributes :id, :latitude, :longitude, :city, :country, :companies, :updated_at

  def companies
    companies = object.ip_ranges.from_last_update(date).map(&:company).uniq
    companies.map do |company|
      SimpleCompanySerializer.new(company)
    end
  end

  def date
    @instance_options[:date] || nil
  end
end
